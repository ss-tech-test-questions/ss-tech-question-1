<?php

/*
The manual approach to reversing items in an array, would be to use use a for loop, 
that re-assigns items in the arrary you want to reverse to the same variable has already been delcared and instantiated.
So for every pass of the loop in which there are items in the array to loop through, (the counter is greater than zero), 
simultaneously unset the current index, destroying the value set in that index's slot, while simultaneously re-assigning that same value, 
to the next index's slot in the array.
*/

//Declare global variable array of fruit.
$arr = ['apple','banana','orange','coconut'];


/*
Initiating the counter variable, using the count function, to count the items in the array of fruit, 
subtracting one from that count, because arrays are based on an index of 0, where the first element of arrays, 
are stored. So while you can count four items in the array, the 4th item is really stored in the third index 
of the array, because the count of the items begins at zeroith index.  Not the first. 

Now with that variable we want the for loop to run as long as that count of items is greater than zero because
at zero, there are no more items in the array.  And finally the for loop needs to reduce that counter variable
down by one as each item is processed so it uses the decrement operator to do so.
*/

for($i=count($arr)-1;$i>=0;$i--){
    
    //debugging or being able to check the state of the variable or array at each step 
    var_dump($arr);
    
    //re-assigns items in an array to an already delcared and instantiated variable
    $arr[]=$arr[$i];
    
    //debugging or being able to check the state of the variable or array at each step 
    var_dump($arr);
    
    //unset the current index, destroying the value set in that index's slot
    unset($arr[$i]);
}
//print the reversed elements of the array
print_r($arr);

###  LOOP PASS #1 ####################################################################################################################

//var_dump1
array(4) {
    [0]=>
    string(5) "apple"
    [1]=>
    string(6) "banana"
    [2]=>
    string(6) "orange"
    [3]=>
    string(7) "coconut"
  }
  //var_dump2
  array(5) {
    [0]=>
    string(5) "apple"
    [1]=>
    string(6) "banana"
    [2]=>
    string(6) "orange"
    [3]=>
    string(7) "coconut"//pointer after pass 1
    [4]=>
    string(7) "coconut"
  }
  ###  LOOP PASS #2  ####################################################################################################################
  
  //var_dump1
  array(4) {
    [0]=>
    string(5) "apple"
    [1]=>
    string(6) "banana"
    [2]=>
    string(6) "orange"
    [4]=>
    string(7) "coconut"
  }
  //var_dump2
  array(5) {
    [0]=>
    string(5) "apple"
    [1]=>
    string(6) "banana"
    [2]=>
    string(6) "orange"//pointer after pass 2
    [4]=>
    string(7) "coconut"
    [5]=>
    string(6) "orange"
  }
 ###  LOOP PASS #3  ####################################################################################################################
  //var_dump1
  array(4) {
    [0]=>
    string(5) "apple" 
    [1]=>
    string(6) "banana"
    [4]=>
    string(7) "coconut"
    [5]=>
    string(6) "orange"
  }
   //var_dump2
  array(5) {
    [0]=>
    string(5) "apple"
    [1]=>
    string(6) "banana"//pointer after pass 3
    [4]=>
    string(7) "coconut"
    [5]=>
    string(6) "orange"
    [6]=>
    string(6) "banana"
  }
   ###  LOOP PASS #4 ####################################################################################################################
  
   //var_dump1
  array(4) {
    [0]=>
    string(5) "apple"
    [4]=>
    string(7) "coconut"
    [5]=>
    string(6) "orange"
    [6]=>
    string(6) "banana"
  }
  //var_dump2
  array(5) {
    [0]=>
    string(5) "apple"//pointer after pass 4
    [4]=>
    string(7) "coconut"
    [5]=>
    string(6) "orange"
    [6]=>
    string(6) "banana"
    [7]=>
    string(5) "apple"
  }

//results of print_r
  Array
  (
      /*
      [0] unset on the final pass because that is where the pointer was. 
      [1] unset after pass 3 that's where the pointer was.
      [2] unset after pass 2 that's where the pointer was. 
      [3] unset after pass 1 that's where the pointer was.
      */
      [4] => coconut //Re-assigned after first pass
      [5] => orange // Re-assigned after second pass
      [6] => banana //Re-assigned after third pass
      [7] => apple //Re-assigned on the final pass. 
  )
  